package ru.toppink.clientservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterDto {

    @Email
    @NotBlank
    @Size(max = 127)
    private String email;

    @Size(max = 127)
    @NotBlank
    private String password;

    @Size(max = 127)
    @NotBlank
    private String passwordRepeat;
}
