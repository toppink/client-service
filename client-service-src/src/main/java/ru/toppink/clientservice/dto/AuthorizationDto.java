package ru.toppink.clientservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthorizationDto {

    private Long id;
    private String uuid;
    private String access;
    private String refresh;
}
