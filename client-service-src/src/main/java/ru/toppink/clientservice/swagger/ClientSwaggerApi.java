package ru.toppink.clientservice.swagger;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.toppink.clientservice.dto.AuthorizationDto;
import ru.toppink.clientservice.dto.LoginDto;
import ru.toppink.clientservice.dto.RegisterDto;
import javax.validation.Valid;

public interface ClientSwaggerApi {

    @ApiOperation(value = "Регистрируем пользователя")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Пользователь успешно зарегистрирован")
    })
    @PostMapping("/public/api/v1/security/register")
    AuthorizationDto register(
        @Valid @RequestBody RegisterDto registerDto
    );


    @ApiOperation(value = "Авторизация пользователя")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Пользователя успешно авторизован")
    })
    @PostMapping("/public/api/v1/security/login")
    AuthorizationDto login(
            @Valid @RequestBody LoginDto loginDto
    );
}
