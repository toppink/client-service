package ru.toppink.clientservice.service;

import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.web.client.HttpClientErrorException;
import ru.toppink.clientservice.dto.KeycloakResponseDto;

public interface KeycloakService {

    KeycloakResponseDto register(String user, String password);

    KeycloakResponseDto login(String user, String password) throws AuthorizationServiceException;
}
