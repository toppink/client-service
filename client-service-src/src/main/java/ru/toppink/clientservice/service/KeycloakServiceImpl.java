package ru.toppink.clientservice.service;

import org.springframework.stereotype.Service;
import ru.toppink.clientservice.dto.KeycloakResponseDto;
import java.util.UUID;

@Service
public class KeycloakServiceImpl implements KeycloakService {

    @Override
    public KeycloakResponseDto register(String user, String password) {
        return KeycloakResponseDto.builder()
                .uuid(UUID.randomUUID().toString())
                .access(
                        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMj" +
                                "M0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2M" +
                                "jM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
                )
                .refresh(
                        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMj" +
                                "M0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2M" +
                                "jM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
                )
                .build();
    }

    @Override
    public KeycloakResponseDto login(String user, String password) {
        return KeycloakResponseDto.builder()
                .uuid(UUID.randomUUID().toString())
                .access(
                        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMj" +
                        "M0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2M" +
                        "jM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
                )
                .refresh(
                        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMj" +
                        "M0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2M" +
                        "jM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
                )
                .build();
    }
}
