package ru.toppink.clientservice.service;

import ru.toppink.clientservice.dto.AuthorizationDto;
import ru.toppink.clientservice.dto.LoginDto;
import ru.toppink.clientservice.dto.RegisterDto;

public interface ClientService {

    /**
     * Зарегистрировать пользователя
     *
     * @param registerDto данные для регистрации
     * @return Данные о зарегистрированном пользователе
     */
    AuthorizationDto register(RegisterDto registerDto);

    /**
     * Авторизовать пользователя
     *
     * @param loginDto данные для авторизации
     * @return Данные о авторизированном пользователе
     */
    AuthorizationDto login(LoginDto loginDto);
}
