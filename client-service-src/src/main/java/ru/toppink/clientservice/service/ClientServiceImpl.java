package ru.toppink.clientservice.service;

import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import ru.toppink.clientservice.dto.AuthorizationDto;
import ru.toppink.clientservice.dto.KeycloakResponseDto;
import ru.toppink.clientservice.dto.LoginDto;
import ru.toppink.clientservice.dto.RegisterDto;
import ru.toppink.clientservice.model.Client;
import ru.toppink.clientservice.repository.ClientRepository;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private KeycloakService keycloakService;

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public AuthorizationDto register(RegisterDto registerDto) {
        if (!registerDto.getPassword().equals(registerDto.getPasswordRepeat())) {
            throw new HttpClientErrorException(HttpStatus.CONFLICT, "Passwords not equals");
        }

        String email = registerDto.getEmail();
        String password = registerDto.getPassword();


        if (clientRepository.countByEmail(email) > 0){
            throw new HttpClientErrorException(HttpStatus.CONFLICT, "User with this email already exists");
        }

        KeycloakResponseDto registerInfo = keycloakService
                .register(email, password);

        Client client = new Client();
        client.setUuid(registerInfo.getUuid());
        client.setEmail(email);

        Client saved = clientRepository.save(client);

        return AuthorizationDto.builder()
                .id(saved.getId())
                .access(registerInfo.getAccess())
                .refresh(registerInfo.getRefresh())
                .uuid(registerInfo.getUuid())
                .build();
    }

    @Override
    public AuthorizationDto login(LoginDto loginDto) {
        String email = loginDto.getEmail();
        String password = loginDto.getPassword();

        Client client = clientRepository.findByEmail(email).orElseThrow(
                () -> new HttpClientErrorException(HttpStatus.NOT_FOUND, "User with email " + email + " not found")
        );

        KeycloakResponseDto loginInfo = keycloakService.login(email, password);

        return AuthorizationDto.builder()
                .id(client.getId())
                .access(loginInfo.getAccess())
                .refresh(loginInfo.getRefresh())
                .uuid(loginInfo.getUuid())
                .build();

    }

}
