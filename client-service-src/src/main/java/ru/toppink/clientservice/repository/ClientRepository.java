package ru.toppink.clientservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.toppink.clientservice.model.Client;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    Integer countByEmail(String email);

    Optional<Client> findByEmail(String email);
}