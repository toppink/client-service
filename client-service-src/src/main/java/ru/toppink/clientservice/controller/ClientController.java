package ru.toppink.clientservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import ru.toppink.clientservice.dto.AuthorizationDto;
import ru.toppink.clientservice.dto.LoginDto;
import ru.toppink.clientservice.dto.RegisterDto;
import ru.toppink.clientservice.service.ClientService;
import ru.toppink.clientservice.swagger.ClientSwaggerApi;
import javax.validation.Valid;

@RestController
public class ClientController implements ClientSwaggerApi {

    @Autowired
    private ClientService clientService;

    @Override
    public AuthorizationDto register(RegisterDto registerDto) {
        return clientService.register(registerDto);
    }

    @Override
    public AuthorizationDto login(@Valid LoginDto loginDto) {
        return clientService.login(loginDto);
    }
}
