package ru.toppink.clientservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public enum Gender
{
    MALE,
    FEMALE
}



