package ru.toppink.clientservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "client")
@NoArgsConstructor
@AllArgsConstructor
public class Client {
    @GeneratedValue
    @Id
    private long id;

    @NotBlank
    @Size(max = 255)
    private String uuid;

    @NotBlank
    @Size(max = 63)
    private String email;

    @Size(max = 63)
    private String name;

    @Size(max = 63)
    private String phoneNumber;

    @Size(max = 63)
    private String city;

    @Size(max = 127)
    private String street;

    @Size(max = 63)
    private String home;

    @Size(max = 63)
    private String flat;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private LocalDateTime birthday;
}
