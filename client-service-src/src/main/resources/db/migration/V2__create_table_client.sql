create sequence hibernate_sequence;

create table client(
    id int8 not null,
    uuid varchar(255) not null,
    email varchar(127) not null,
    name varchar(63),
    phone_number varchar(63),
    city varchar(63),
    street varchar(127),
    home varchar(63),
    flat varchar(63),
    gender varchar(63),
    birthday timestamp
)